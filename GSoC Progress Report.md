# GSoC progress report

## Introduction
This GSoC I worked on two projects, namely **Command Palette**<sup>1</sup> and **Macros**.

_**Command Palette**_ is in simple terms, a *search utility* for enabling user to search all operations the program has to offer, and saving them from a slow process of digging through menus.
- [GitLab Merge Request, Command Palette](https://gitlab.com/inkscape/inkscape/-/merge_requests/2072)

_**Macros**_ are combined actions/operations, these are useful for executing common tasks that need to be repeated many times.
- [ GitLab Merge Request, Macros](https://gitlab.com/inkscape/inkscape/-/merge_requests/2190)
## Progress
### Command Palette
#### Searching
Current Temporary algorithm used is substring search

![Command Palette Search Recording](./images/cp-search.gif)
#### Executing
Press `Enter/click` to execute/perform

![Command Palette Execute Recording](./images/cp-execute.gif)
#### Recent Files
User can also search recent files

![Command Palette recent file search Recording](./images/cp-recent-files.gif)
#### Untranslated labels and full action name
Untranslated labels enable users of non-english UI, to help users that have languages than them. Even if Labels are turned off, english search works.

Actions can also be used from the command line, however to find them you can use command palette when you are experimenting with the commands to run.

![Command Palette options Recording](./images/cp-options-enable.gif)
![Command Palette options visible Recording](./images/cp-untranslated-lable-visible.gif)

Labels in smaller fonts are the untranslated i.e. english. In far right cryptic looking names are, action names.
#### History
Access history by pressing up and down keys.

![Command Palette history feature Recording](./images/cp-history.gif)

### Macros
#### Creating macros
![Creating macros recording](./images/macro-create.gif)
#### Creating group
![Creating groups recording](./images/group-create.gif)
#### Deleting group/macro
![Deleting macros/groups recording](./images/macro-delete.gif)
#### Drag and Drop
Drag and Drop feature helps users to easily change group of a macro. To use this users just have to click and hold a macro and drop it on any group or one of it's child.

![Drag and Drop macros recording](./images/dnd-macros.gif)

#### Renaming Macros
![Reanaming macros/groups recording](./images/rename.gif)

#### Storing macros
![storing macros/groups recording](./images/storing.gif)

#### UI layout and arrangement
![layout recording](./images/layout.gif)

## TODO
### Command Palette
- Shortcut support
  - Depends on [shortcuts branch](https://gitlab.com/inkscape/inkscape/-/tree/shortcuts)
- Supporting **multiple parameters in actions** and popping dialogs with validated input fields
  - Refactoring for new system done, adding data and dialogs popping left
- Better string matching algorithm
### Macros
- Multiple selections to delete, import and export
- Recording and Executing instructions
  - Depends on supporting **multiple parameters in actions**

-------------
**[ 1 ]:** Command Palette, name not finalised, [Standard Naming and Shortcut thread](https://gitlab.com/inkscape/inbox/-/issues/3217)
