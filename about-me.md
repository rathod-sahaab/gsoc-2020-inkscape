# Command palette and Command stack

GSoC 2020 Proposal

## About Me

Hi, I am **Abhay Raj Singh**, a sophomore student of NIT Hamirpur, Bharat(India), I am
currently pursuing Bachelors in Materials Science and Engineering. I am experienced in
C++98/11/14, python 3, full-stack development and a bit of Dev Ops. I have had formal training in
C++98 in my high school, in the rest of the skills I am self-taught.

I came to know about open-source when I started college and have contributed to and used
open-source projects since then. Last year I contributed 5+ MRs during Hacktober Fest,
fixing bugs of and adding features to C++ and Python libraries I use. During the same period,
I started contributing to Inkscape extensions and fixed a bug in the _interpolate extension_.

After which I got the chance to work on a full-stack website with an unconventional, optimized
architecture _(at least for us)_ for organizing a technical fest in our college.

I have used Inkscape for creating and editing graphics for various events organized in our
college and for creating some wallpapers/game/web assets.

### Languages/Syntaxes I know

- Programming
	- C++98/11/14
	- Python 3
	- JavaScript (ES5/ES6+)
- Markup
	- Markdown 
	- HTML5
	- Pug
- Style Sheets
	- CSS3
	- Less

### Contact info

abhayofficialone@gmail.com <br>
**rathod-sahaab** (chat.inkscape.org, [GitLab](https://gitlab.com/rathod-sahaab), [GitHub](https://github.com/rathod-sahaab)) <br>

### Plans for summer

- Learn in-depth about **data structures and algorithms**
- Learn parallel processing and concurrent processing concepts.
- Learn **flutter** for mobile app development
- Invest in my **fitness**.
- Learn to play harmonica :)

### Other programming projects

- [cppm](https://github.com/rathod-sahaab/cppm) A library for Netpbm family images
- [Happy Tux](https://rathod-sahaab.github.io/happy_tux/) A web-based game, click the
  happiest tux
- [Compile Time Cpp](https://github.com/rathod-sahaab/compile-time-cpp) How to make your C++ compiler write your code
- [Web Tricks](https://github.com/rathod-sahaab/web-tricks) How to make your C++ compiler write your code

I am a member of a Society named **Indian Society for Technical Education(ISTE)**
students' chapter NIT Hamirpur. Together we do many awesome projects which are OpenSource.

- Embedded
	- [CSC19](https://github.com/istenith/CSC19) Projects for children Science Congress
		- [Custom input devices (Macro Electronics)](https://github.com/istenith/CSC19) we made different types of keys touch/no-touch/Push on Arduino platform using C++, and features like inheritance and Template made the job easy.

- **Web development** 
	- [Prodyogiki(2020)](https://istenith.com/prody) A website with unconventional
  architecture, that hugely minimizes server-side rendering, while still using all good features.
	- [Pahal](https://istenith.com/pahal) A website to help new students in the process
  of registrations.

Visit [my GitHub profile](https://github.com/rathod-sahaab/web-tricks) for more projects.

### Contribution to other Open Source projects

In last year's Hacktober Fest I contributed to these projects that I use.

- [C++ Pipes](https://github.com/joboccara/pipes): Pipes are small components for writing
  expressive code when working on collections. Pipes chain together into a pipeline that
  receives data from a source, operates on that data, and send the results to a destination.
	- [Added feature stride](https://github.com/joboccara/pipes/pull/24)
	- [Simplified code](https://github.com/joboccara/pipes/pull/25)

- [cxxopts](): CLI arguments parser for C++
	- [Fixed bug](https://github.com/jarro2783/cxxopts/pull/205)

- Other Projects
	- [CoolKit](https://github.com/srbcheema1/CoolKit) A CLI program to help in competitive programming submissions and testing.
		- [Improved portability across different distros](https://github.com/srbcheema1/CoolKit/pull/8)

### Tools I use

- Editors
	- NeoVim(primary)
	- VSCodium
- Extensions
	- Coc.nvim (platform for many features)
	- code-fmt (code formatting)
- Debuggers
	- LLDB
	- valgrind
- Automation
	- Webpack
	- npm
- Others
	- Manjaro Linux
	- Inkscape
	- Firefox Developer Edition

## Inkscape Questions

**Q. When did you first hear about Inkscape?** <br>
December 2018, I was looking for a software that would run on Linux for work at my
college society.

**Q. What kind of drawings do you create with Inkscape?** <br>
I mostly make simple posters for events conducted by the college teams I am in (currently 3).
Also, I have made many illustrations, game assets, resumes, logos, bulk-editing,
maps(thanks to OpenMaps), etc.

![Drawings Image](./images/inkscape-drawings.png)

**Q. Describe your participation in our community.**

- I have reported some bugs (#983, #458)
- I have actively participated in Inkscape community chats helping some users with their problems.
- I have contributed ideas to feature requests and confirmed some bug reports.

**Q. Describe your contributions to the Inkscape development.** <br>
I have now 5 MRs merged in the codebase

- Fixed issue [extensions#121](https://gitlab.com/inkscape/extensions/issues/121) - [!140 Fixed first object style was set to last object style](https://gitlab.com/inkscape/extensions/-/merge_requests/140)
- Fixed issue [#690](https://gitlab.com/inkscape/inkscape/issues/690)
  and [#689](https://gitlab.com/inkscape/inkscape/issues/689) - [!1446 Fix and Refactor spinbutton toolitem](https://gitlab.com/inkscape/inkscape/-/merge_requests/1446)

- Fixed an issue faced by me - [!1414 fixed un-resizing gradient menu](https://gitlab.com/inkscape/inkscape/-/merge_requests/1414)

- Fixed issue [#926](https://gitlab.com/inkscape/inkscape/issues/926) - [!1420 fixed opacity-blur padding at end](https://gitlab.com/inkscape/inkscape/-/merge_requests/1420) - [!1426 Fixed vertical expand issue on gradient](https://gitlab.com/inkscape/inkscape/-/merge_requests/1426)

Under Review and WIP MRs

- [!1423 WIP: Fixed arithmetic operations not working spin buttons](https://gitlab.com/inkscape/inkscape/-/merge_requests/1423)

**Q. In exactly two sentences, why should we pick YOU?** <br>

- I truly believe in the idea of open-source _(by the people, for the people, and of the people)_,
  it makes me feel **independent**, that I can do anything with some skills and some good friends, like you ^-^.
- I believe good software is more than just the one which _"works"_, it should be intuitive
  for the user, maintainable, extensible and fun for the developers and as performant and robust as it can be.

Apart from that, I have skills and will to contribute to Inkscape and I don't feel like
stopping anytime soon :) as I use Inkscape.

### Other GSoC organization I am applying to

- BRL CAD: OGV
	- Finalizing to deployable state frontend and backend

- Webpack 
	- Webpack bundle: make webpack run on browsers. 
	- Make executable files for webpack-cli

