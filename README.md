# My GSoC 2020 Proposal for Inkscape

- [about-me.md](./about-me.md) contains all information about me and answers to _inkscape questions_.
- [idea.md](./idea.md) contains the idea I wish to propose.
- [Application.md](./Application.md) is the combined application containing both.

Please feel free to make a issue if you want more features, different features, spot any errors etc. and thanks for that!!

To split `Application.md` I use following command.
```
csplit --suppress-matched Application.md /TL\;DR/ '{*}' && mv xx00 about-me.md && mv xx01 idea.md
```
Thanks to [this stackexchange answer](https://unix.stackexchange.com/a/281366/328703)

I use **TL;DR** as marker, so that the reader can skip to idea part just by searching **TL;DR** (common on internet).
