
## Problem

Inkscape comes with a plethora of features out of the box, and functionality can be extended
even more by using various extensions. These features and extensibility have made Inkscape
the wonderful software it is today.

But, these features come with a cost, _i.e._ difficulty accessing these features.
Currently, operations have to be search through **highly nested** _(Extensions >
Render > Barcode > QRCode)_ and **huge** _(Extensions > Render > 34items)_ drop-downs.
Also, you have to **remember** where the option was.

Keyboard shortcuts don't solve the problem due to their **learning curve**, **system shortcuts**,
and the limit on the number of keys that can humanly be pressed.

## Solution: Command palette and stack

![Demo Image](./images/command-pallete-stack.png)

### Command Palette

Command Palette is an **interactive** search bar, that can search any operation _(extensions, edits,
filters, etc.)_ and will support auto-completion.

_To toggle command palette, press `` Ctrl+` ``_

**Example:**

_To interpolate (@extensions)_

<pre>
+--------------------------------------------
|  Inter|
+-------------------------------- suggestions
| <b>Inter</b>polate: (Dialog)                 #1136
| <b>Inter</b>polate: (Command)                #1137
| <b>Inter</b>cept:   (Command)                #1596
</pre>

##### Numeric codes

<pre>
+--------------------------------------------
|  #1136|
+-------------------------------- suggestions
| Interpolate: (Dialog)                 <b>#1136</b>
</pre>

**Numeric codes** search using numbers and enable users of different locale(language) follow,
for example, tutorials of a user with some other locale.

Numeric codes can be turned off and on from preferences.

#### Commands

_Commands are useful for a copy-pastable set of instructions and keyboard lovers like
me :), however, dialogs can generate the command for you_

Commands can also wait for input like select 2 paths _(as in AutoCAD)_ this helps to
repeat with scripts/copy-pasted commands/macros.

- To cycle through suggestions (once they show up in drop-down), one can press `Tab` for
  forward and `Shift+Tab` for backward cycling.
- To select the operation press `Enter`.
- If you selected command option instead of Dialog
- To iterate over arguments/value fields press `Tab`.
- Press `Enter` to execute the command.
```
+-------------------------------------------- 
| Interpolate [steps: 5] [exponent: 0.0] ...
+--------------------------------------------
```

- When you are copy-pasting a set of say 5 commands (from command history)
- To authorize one command press `Enter`
- To authorize command once press `Shift+Enter`
  _(generally for value-dependent operations like interpolation)_
- Command stack realm (guidance needed)
- Press `Ctrl+Enter` after pasting and then loop over commands but in the first cycle
  authorize them as needed.

_**Note:** GUI equivalents to these will be provided too(command stack)._ <br>
_**Note:** Default shortcuts can change, and customization via preferences is aimed._

### Extra access points

#### CLI

Press `Ctrl+$` or press `$` if already in command palette to enter Inkscape CLI.
This would be especially useful for the devs and testers working on enhancing CLI support.

So, that they can see the effectiveness of command real-time in Inkscape.

#### Python support

There is a PR intending to embed python within Inkscape, we will also provide a
platform to run python commands straight from Command Palette.

Shortcuts: `Ctrl+>` and `>` _(like CLI)_

### Problem

There is no way to visualize performed commands/history. Some operations aren't visually
prominent enough, hence sometimes the user is unsure if they were executed or not.

Repeating commands with the command palette will be a pain due to the lack of space.

Most users have a style they prefer and work mostly with that. Most of the styles have
some set of commands that are needed together frequently. These are often called
**"Macros"** in some softwares. (Vim/NeoVim, nano, VSCode)

### Command Stack

History of the commands executed and (maybe) the preview of objects on which
command was executed*(like in GIMP)*. Undo will move command marker (instruction pointer)
back, Redo: forward, (maybe add the feature of branching like in `git`).

Command Stack provides the feature to edit macros using intuitive GUI.
We will also be able to make macros out of executed commands, aka history.

Shortcuts (customisable):
- `.` to repeat the last command
- `,` to walk over commands between the "macro start" and "macro end" markers
- `Shift+,` to skip a command when executing a macro

- `q` to start recording a macro and end recording a macro. _(Vim/NeoVim inspired)_
- `@` to select which macro to execute

## Time Table

**Before and during Community bonding period**

- Finalizing GUI design by interacting with `#team_ux`
- Know about locale constraints that may cause problems for some users and finding solutions.
- Learn more about codebase especially
	- Gio::Actions
	- verbs
	- history dialog
	- Canvas
	- Document
	- Extension framework and `.inx` files

---

**Coding starts**

- **[0]** Implementing the GUI code (Command pallete)
- **[1]** Adding and testing key bindings on test commands
- **[2]** Indexing: Adding support for `Gio::Actions`, `Verbs` (verbs non-blocking)
- **[3]** Indexing: Adding support for extensions using `.inx` files.<sup>1</sup>
- **[4]** Ask and wait for selections and inputs feature (select paths, group, objects, movement, scale, etc.)
- **[5,6]** "Dialog to command" feature support (essential, non-blocking) - extensions (using `.inx` files) - built-in commands and operations (not Gio::Actions or Verbs)

---

- **[7]** Repurpose History dialog's GUI (add features) to function like command stack
- **[8, 10]** Macros Editing and export feature
	- Make macros from history
	- Macro specialization and customization (change a **value** for this instance, of an already recorded macro)
	- Target detection<sup>2</sup>
- **[11]** Integration with Command palette so users can search saved macros

---

- **[12+]** Documentation and Tutorials for users (essential, non-blocking)

**(1):** Extensions aren't converted to actions yet, hence `.inx` files are needed to be used.

**(2): Target detection:** <br>
During editing(in the scope of a macro) an object may be selected and deselected many times.
A macro may contain two or more objects. Target detection will ensure the user is only
asked distinct objects.

Hence, it ensures that macros are like functions where input objects have to be passed only
once. You can also preview the operation which will be applied to the object you need to
select so that you can make an in formed decision.

**Select by style** feature would also be helpful so that you can drag and select all concerned objects in one go and don't have to remember order.

## Stretch Goals

These are some other features that I want to add in Inkscape having the motivation of the main project i.e. **"better UI"**.
Which I will try to complete after the **main project** is completed, _i.e._ during GSoC period and certainly after GSoC.

- [Multple tab interface](https://gitlab.com/inkscape/inbox/-/issues/2061) instead of current "multiple window" interface to open new files.
- [New layer new outline color](https://gitlab.com/inkscape/inbox/-/issues/1716)
- [Object origins](https://gitlab.com/inkscape/inbox/-/issues/1644) UI.

## Usefulness

By this feature, we can ease the development process for most of Inkscape GUI users.
Also, easing the process to search features/extensions will motivate them to use them rather than time-consuming DIYs.

On the other hand, it will make the life of pros easier by helping them easily find the tool they need from the hay-stack they have accumulated over time.

**Composition** of actions is the easiest way to, "not to repeat" oneself. (C++20 Ranges aim for that.). Command stack providing the functionality would make using Inkscape fun, especially for the users using it for technically accurate drawings and illustrations.

Softwares that already have similar features to command palette
- VS Code
- Cinema 4D
- AutoCAD [YouTube](https://www.youtube.com/watch?v=KZoCJ-mUffs)

Macros
- Vim / NeoVim
- Nano

### Use cases

YouTube tutorial: Only added jumps to concerned times

- [Problem 1](https://youtu.be/ywInKVezX6o?t=347)
	1. Start Recording macros
		1. Select object/letter<sup>1</sup>
		2. `Ctrl+C`: Copy selected
		3. Duplicate rectangle (Duplication will happen for the same rect always) - alternative: Create a rectangle
		4. Paste Width
		5. Select first object<sup>2</sup>, Shift select rectangle
		6. Align(first selected): Center on the vertical axis
		- [1]: asked every time or press `Ctrl + e` to select this **every time** aka don't ask again
		- [2]: ease(UX): won't be asked again when similar to one of the objects selected before
		- Undos are respected and will be optimized
	2. Execute Macro: `,` to proceed forward, `Shift+,` for back
		- `,`: **Select object1**
      		- _**if** in watch mode else will be executed automatically_
      			- `, , , , ,`


- [Problem 2](https://youtu.be/ywInKVezX6o?t=513) combining two parts in one
	1. Start recording macro
		1. Select obj1(letter)
		2. Select obj2
		3. Align(last selected): Center vertically, center horizontally<sup>1</sup>
		4. Enter Perspective transform<sup>2</sup> mode, on obj1<sup>1</sup> (`ESC` to exit)
		5. Make changes (not recorded, ask)
		6.
		- [1]: We can support "same styles" to select respective to object and object on which path effect has to be applied.
		- [2]: LPE can be edited afterward hence it makes sense to just apply the LPE and the user can play with it later(in some cases).

	2. Execute macro
		1. select obj1(asked)
		2. select obj2(asked)
		3. Edit Perspective(press `ESC` after editing to exit LPE and continue)

**Reminder:** We, also can edit macros through GUI.

Just do the process once, then, use command stack(the history) to mark the start, the end, and for the operations mark "ask every time or not".
Of course, you can use the command palette for faster searching and execution.

Mostly for simple things you would use `.` to repeat a command in history several times.
So, instead of `Edit > Paste Size > Paste Width` many times,
you can do that once and press `.` to repeat the last command again and again.

Or suppose you need to place trees 600mm apart do it once repeat as many times as you like.

### Hack

Macros can also be used as follow along tutorials! Most useful for supplementing Inkscape tutorials (`Help > tutorials`) and third-party tutorials for people with high priced internet connections.

## Other projects at Inkscape that I am interested in

- [Renderer improvements](https://wiki.inkscape.org/wiki/index.php?title=Projects#Renderer_improvements)
  - Experimental GPU rendering support (e.g. cairo-gl or something vulkan-based). Should be compile-time option defaulted to off.

### Refactoring

- Remove livarot, moving any needed functionality into lib2geom or other files.
- Using smart pointers instead of raw pointers, to depend less on Garbage Collection.
